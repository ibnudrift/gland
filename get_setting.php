<?php 
// function get database $dbcon
$mysqli = new mysqli($dbcon['host'], $dbcon['user'], $dbcon['pass'], $dbcon['db']);
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
// mysqli_select_db($dbcon['db']);

    function getSetting($lang, $mysqli){

        $squery = $mysqli->query("select * from setting");
        $data = array();
        while ($result = mysqli_fetch_assoc($squery)) {
            // print_r($result); exit;
            if ($result['dual_language']=='y') {
                $v = getSettingModel($result['name'], $lang, $mysqli);
                $data[$result['name']]=$v['value'];
            } else {
                $data[$result['name']]=$result['value'];
            }       

        }

        return $data;
    }


    function getSettingModel($setting_name, $language_code, $mysqli)
    {
        $setting_id = mysqli_fetch_assoc($mysqli->query("SELECT * FROM setting where name='$setting_name'"));
        $setting_id = $setting_id['id'];
        $language_id = mysqli_fetch_assoc($mysqli->query("SELECT * FROM language where code='$language_code'"));
        $language_id = $language_id['id'];

        $model = mysqli_fetch_assoc($mysqli->query("SELECT * FROM setting_description where setting_id='$setting_id' and language_id='$language_id'"));

        return $model;
    }

    function getModBlog($language_code, $mysqli)
    {
        $language_id = mysqli_fetch_assoc($mysqli->query("SELECT * FROM language where code='$language_code'"));
        $language_id = $language_id['id'];

        $model = ($mysqli->query("SELECT tb1.*, tb2.language_id, tb2.title, tb2.content  FROM `pg_blog` tb1 LEFT JOIN `pg_blog_description` tb2 ON tb1.id = tb2.blog_id where language_id='$language_id' ORDER BY tb1.id DESC;"));

        $data = array();
        while ($result = mysqli_fetch_assoc($model)) {
            $data[ $result['id'] ] = $result;
        }
        return $data;
    }

    function getModBlog_home($language_code, $mysqli)
    {
        $language_id = mysqli_fetch_assoc($mysqli->query("SELECT * FROM language where code='$language_code'"));
        $language_id = $language_id['id'];

        $model = ($mysqli->query("SELECT tb1.*, tb2.language_id, tb2.title, tb2.content  FROM `pg_blog` tb1 LEFT JOIN `pg_blog_description` tb2 ON tb1.id = tb2.blog_id where language_id='$language_id' ORDER BY tb1.id DESC LIMIT 3;"));

        $data = array();
        while ($result = mysqli_fetch_assoc($model)) {
            $data[ $result['id'] ] = $result;
        }
        return $data;
    }

    function getRandom()
    {
        $randm = 'rand-'.rand(1000,9999);
        return $randm;
    }

    $set_random = getRandom();

    $lists_activity = array(
    	1 => array(
                'thumb' => 'activity-1.jpg',
                'pict' => 'activity-1.jpg',
                'title' => 'PROFESSIONAL SURFING',
                'desc' => '<p>Surfers from all over the world have recognized how fantastic the waves are in G-Land and how exotic the location of G-Land. Lots of professional surfers are willing to spend more than 20 hours traveling to reach the G-Land. The G-Land, nicknamed &quot;The Seven Giant Waves Wonder&quot; has big, consistent and high waves. It&#39;s quite common to see waves with heights reaching up to 6 meters tall.</p>

<p>G-Land waves are very long, barreling left hand reef/point break breaks along the east side of Grajagan Bay. G-Land waves are considered one of the world&#39;s best left hand waves. G-Land&#39;s wave becomes shallower and critical the further down the point one rides the wave. It is one of the most consistently rideable waves in the world in season, with offshore tradewinds and often plentiful swell between the months of April to October. The G-Land surf break has been divided up into several sections. First one, at the top of the point is called &quot;Kongs,&quot; which breaks up to several hundred metres in length, and can hold quite large sizes (from about 2 to 12 feet+, Hawaiian scale). It is not usually a barrel, nor&nbsp; genuinely world-class, but more a series of takeoff zones with some long wall sections, although it can also barrel on occasions. This is also where surfers can find the &#39;key-hole&#39; which is a section of the reef that allows a more forgivable paddle out. This section picks up a lot of swell, and is rarely less than 3 feet, and can be a saviour when the rest of the point is too small. This wave can sometimes link up with the next section called &quot;Moneytrees.&quot; Moneytrees works from about 2 to 10 feet (Hawaiian scale, or about 4 to 20 feet wave faces), usually breaking over several hundred metres, and is a long, testing, barreling, world-class wave. The barrels become more critical the lower the tide and the larger the swell. Moneytrees may also occasionally link up with the next section called &quot;Speedies,&quot; with an outside takeoff section between the two called &quot;Launching Pads.&quot; &quot;Launching Pads&quot; can catch the surfer offguard, as it can break a significant way out to sea in larger swells. &quot;Speedies&quot; (named after how fast the wave breaks) is the heaviest wave at G-Land, but can be a perfect, very round barrel for several hundred metres, rideable from about 2 to 8 feet+ (Hawaiian scale). It is not common to ride a wave more than about 300&ndash;400 metres at G-Land, even though the section of the point where rideable waves break is considerably longer (over 1 km long), because the waves usually don&#39;t link up with each other.</p>

<p>Jack&#39;s Surf Camp at G-Land is the closest accommodation to G-Land&#39;s favorite surfing location. When other surfers have to spend up to 1km more, guests staying at Jack&#39;s Surf Camp only need to walk out through the camp&#39;s gate to get off heading towards the best surfing location in G-Land.</p>
<p class="hidden-xs" style="font-size:20px;"><a style="font-size: 15px;" href="https://magicseaweed.com/G-Land-Surf-Report/563/" target="_blank">Click here</a> to view G-Land surf report and forecast</p>
        <p class="visible-xs" style="font-size:20px;"><a style="font-size: 15px;" href="https://magicseaweed.com/G-Land-Surf-Report/563/" target="_blank">Click here</a> to view <br>G-Land surf report and forecast</p>
        ',
                'list_pict' => array('surf-1.jpg', 'surf-2.jpg', 'surf-3.jpg', 'surf-4.jpg', 'surf-5.jpg', 'surf-6.jpg', 'surf-7.jpg', 'surf-8.jpg'),
                ),
            array(
                'thumb' => 'tiger-tracks.jpg',
                'pict' => 'tiger-tracks.jpg',
                'title' => 'Tiger Tracks surfing',
                'desc' => '<p>1 hour from G-land Jack&#39;s Surf Camp is the much more relaxed surfing for beginners or intermediates to enjoy smaller waves called Tiger Tracks. Its an easy, perfect, mellow, right hand barrel from 2 to 4 feet breaking over about 70m, that can also break left. The wipeouts are soft and the wave is whackable. Only works a few hours either side of high tide, as the reef gets exposed on low tide. Tiger Tracks is a good soft alternative to G-Land, it will only be about half the size of the point, depending on swell direction, although, if the point is small (head high) then tigers is also the same size. Doesn&#39;t handle more than about 4 feet, or 5 people in the water, but there are other good left and right reef breaks right next to it. Look to the right of the main peak and you&#39;ll see another right.</p>',
                'list_pict' => array('tiger-track-surfing2.jpg'),
                ),
            array(
                'thumb' => 'activity-2.jpg',
                'pict' => 'activity-2.jpg',
                'title' => 'FISHING & KAYAKING',
                'desc' => '<p>G-Land&#39;s guests Jack&#39;s Surf Camp can rent local boats to go fishing in the ocean. You can take your catch to the kitchen of G-Land Jack&#39;s Surf Camp and then ask for it to be cooked upon your desired taste. For guests of G-Land Jack&#39;s Surf Camp who love Kayaking activities, you can go to the north section of G-Land Plengkung beach to find a more calm wave and enjoy the beautiful scenery.</p>',
                'list_pict' => array(),
                ),
            array(
                'thumb' => 'activity-3.jpg',
        		'pict' => 'activity-3.jpg',
        		'title' => 'OFFROAD ADVENTURE',
                'desc' => '<p>For the guests of G-Land Jack&#39;s Surf Camp who is a lover of Offroad Adventure, you will be delighted with a very challenging offroad tracks stretching for more than 10km. Guests of G-Land&#39;s Jack&#39;s Surf Camp can contact the staffs to make offroad vehicle bookings or hire a guide and arrange everything.</p>',
                'list_pict' => array('offroad2.jpg', 'offroad3.jpg'),
        		),
            array(
                'thumb' => 'activity-4.jpg',
                'pict' => 'activity-4.jpg',
                'title' => 'TRAIL RUN',
                'desc' => '<p>The long and twisting G-Land beach has various contours of sands, cliffs and corals. Guests can walk on the forrest path ways that are surrouned with trees. This is a perfect setting for nature lovers to do Trail Run or just walk down enjoying the natural beauty.</p>',
                'list_pict' => array('trail-run2.jpg'),
                ),
            array(
                'thumb' => 'WAVE-WATCHING.jpg',
                'pict' => 'wave-watching-gland.jpg',
                'title' => 'WAVE WATCHING',
                'desc' => '<p>G-Land Jack&#39;s Surf Camp got a massive sized Watch Tower which can be used by surfers to chill, hang out and watching rolling waves from heights. Surfers can have the best spot to enjoy the afternoon while discussing their experiences when surfing at G-Land. With the view facing the sunset, it&#39;s just perfect.</p>',
                'list_pict' => array('wavewatch2.jpg'),
                ),

            array(
                'thumb' => 'SUNSET-YOGA.jpg',
                'pict' => 'yoga.jpg',
                'title' => 'SUNSET YOGA',
                'desc' => '<p>Guests of G-Land Jack&#39;s Surf Camp can use the watch tower to be the comfortable place to perform various Yoga rituals. The roar of the waves that sounded from a distance will make you feel calm and relaxed. So bring your matt and your Yoga suit!</p>',
                'list_pict' => array('yoga2.jpg', 'yoga3.jpg'),
            ),
            array(
                'thumb' => 'PING-PONG-AND-POOL.jpg',
                'pict' => 'pool.jpg',
                'title' => 'PING PONG AND POOL',
                'desc' => '<p>Everyone is a serious Ping Pong player at G-Land Jack&#39;s Surf Camp. Once you put your hands on it, you just can&#39;t stop playing. There&#39;s also a pool table for guests to just have a quality time together at a very relaxed and casual athmosphere.</p>',
                'list_pict' => array('pool2.jpg', 'pool3.jpg'),
                ),
            array(
                'thumb' => 'CULINARY-FIESTA.jpg',
                'pict' => 'culinary.jpg',
                'title' => 'CULINARY FIESTA',
                'desc' => '<p>G-Land Jack&#39;s Surf Camp got a reputation when it comes to food. Our kitchen will bring you the best local Indonesian food that&#39;s exotic and complex. The kitchen will also serve you western food menu selections at a high standard.</p>',
                'list_pict' => array('culinary2.jpg', 'culinary3.jpg'),
                ),
            array(
                'thumb' => 'WILDLIFE-ENCOUNTER.jpg',
                'pict' => 'wildlife.jpg',
                'title' => 'WILDLIFE ENCOUNTER',
                'desc' => '<p>G-Land Jack&#39;s Surf Camp can witness a pack of deers passing by in front of the gate of the surf camp or see monkeys living in the trees inside the area of the Surf Camp. For animal lovers, it will be a very fun and complete experience when staying at G-Land Jack&#39;s Surf Camp.</p>',
                'list_pict' => array(),
                ),
            
    );


    $lists_activity2 = array(
        1 => array(
                'thumb' => 'sadengan-cvr.jpg',
                'pict' => 'sedangan-1.jpg',
                'title' => 'SADENGAN SAVANA',
                'desc' => '<p>Sadengan is part of Alas Purwo (Purwo Forest) which is a savanna. What`s interesting about Sadengan is exotic animals scattered in the 80HA area. If you travel there, you can find a herd of deer that is protected. Sadengan is open to the public for various educational activities, as well as camping. You can spend the night and enjoy the serene and mystical atmosphere of the Purwo forest.</p>',
                'list_pict' => array('sedangan-2.jpg', 'sedangan-3.jpg', 'sedangan-3.jpg'),
                ),
            array(
                'thumb' => 'puragisaloka-cvr.jpg',
                'pict' => 'puragisaloka-1.jpg',
                'title' => 'GIRI SALOKA TEMPLE',
                'desc' => '<p>Still in the Alas Purwo area (Purwo Forest), you can visit the cultural and religious sites of Pura Luhur Giri Saloka. This temple was found still buried in the ground. In the history of local culture, it is written that Pura Giri Saloka could be the origin of the island of Java. So this temple is also called by the name of Pura Kawitan or can be interpreted as the temple of the beginning</p>',
                'list_pict' => array('puragisaloka-2.jpg', 'puragisaloka-3.jpg', 'puragisaloka-4.jpg'),
                ),
            array(
                'thumb' => 'guaistana-cvr.jpg',
                'pict' => 'guaistana-1.jpg',
                'title' => 'ISTANA CAVE',
                'desc' => '<p>If you visit Alas Purwo, you will indeed find a lot of things. One of them is a cave. Actually, there are many caves in the Alas Purwo area. The most famous name is Goa Istana. Apart from being easily accessible, this cave also holds various historical and mystical stories.</p>',
                'list_pict' => array('guaistana-2.jpg','guaistana-3.jpg','guaistana-4.jpg'),
                ),
            array(
                'thumb' => 'IJEN-CRATER.jpg',
                'pict' => 'IJEN-CRATER.jpg',
                'title' => 'IJEN CRATER',
                'desc' => '<p>The Ijen volcano complex is a group of composite volcanoes in the Banyuwangi Regency of East Java, Indonesia. It is inside a larger caldera Ijen, which is about 20 kilometres wide. The lake is recognised as the largest highly acidic crater lake in the world. It is also a source for the river Banyupahit, resulting in highly acidic and metal-enriched river water which has a significant detrimental effect on the downstream river ecosystem.</p>',
                'list_pict' => array('Kawah-Ijen-Resize1.jpg', 'Kawah-Ijen-Resize2.jpg', 'Kawah-Ijen-Resize3.jpg'),
                ),
            array(
                'thumb' => 'GREEN-BAY.jpg',
                'pict' => 'GREEN-BAY.jpg',
                'title' => 'GREEN BAY',
                'desc' => '<p>Teluk Hijau or Green bay is located in Pesanggaran District , precisely in Sarongan village. It located about 90 km to south of Banyuwangi town. To reach this beach from Banyuwangi we just follow the directions to Pesanggaran-Sarongan-Sukamade, that are still one lane of route to Sukamade beach, Merubetiri National Park.</p>
                        <p>Green Bay is typical white sandy beach and the sand is find and it easily embedded in the skin. The bay also has panoramic view with green sea water inside and 8 meter high of waterfall. For those who give visit to this bay, the vehicles can be parked near Rajagwesi beach then we can walked to Green bay as far as +/- 2km. There is also a parking spot closer to the Green bay, but the roads are inadequate.</p>',
                'list_pict' => array('teluk_hijau-resize1.jpg', 'teluk_hijau-resize2.jpg', 'teluk_hijau-resize3.jpg'),
                ),
            array(
                'thumb' => 'RED-ISLAND.jpg',
                'pict' => 'RED-ISLAND.jpg',
                'title' => 'RED ISLAND',
                'desc' => '<p>Red Island beach located at the foot of Mount Tumpang Pitu. It&rsquo;s a beach resort located in Banyuwangi southern tip and have a unique form of a small mountain in the middle of the coast. This place get its name from the red color of its soil. In the south part of the island we can enjoy a beautiful sunset in the evening. About 50 meters in west of the port, there&rsquo;s a big total-fresh fish auction.</p>',
                'list_pict' => array('red_island-resize2.jpg', 'red_island-resize3.jpg'),
                ),
            array(
                'thumb' => 'JAGIR-WATERFALL.jpg',
                'pict' => 'JAGIR-WATERFALL.jpg',
                'title' => 'JAGIR WATERFALL',
                'desc' => '<p>Jagir waterfall is located in Kampung Anyar Village, Banyuwangi East Java. It is only 15 minutes drives from the town. To get this waterfall you have to walk only 10 minutes from the parking area through the path that will lead you there. Jagir waterfall also knows as Twin Waterfall, because in this area you will see there are two waterfalls that have the same form. It is about 25 m high. The water is coming out from the ground that means it is very clean and fresh. It has a natural swimming pool where you might swim while enjoying the waterfalls and surrounding.</p>',
                'list_pict' => array('jagir_waterfall-resize1.jpg', 'jagir_waterfall-resize2.jpg'),
                ),

            array(
                'thumb' => 'thumb-alas-purwo.jpg',
                'pict' => 'alas-purwo.jpg',
                'title' => 'Alas Purwo Forrest',
                'desc' => '<p>G-Land Jack&#39;s Surf Camp is surrounded by a National Forest called &quot;Alas Purwo&quot; forest. So if you&#39;re up for an adventure to the forest, you&#39;re on the right place. With an area of 434 km2, the forest is made up of mangroves, savana, lowland monsoon forests, a cave called &quot;Goa Istana&quot; and there&#39;s also a mount named mount Linggamanis (322m) located in this national park.</p>',
                'list_pict' => array('alas-purwo2.jpg', 'alas-purwo3.jpg'),
                ),
            array(
                'thumb' => 'thumb-hindu-temple.jpg',
                'pict' => 'hundu-temple.jpg',
                'title' => 'Hindu Temple',
                'desc' => '<p>There&#39;s a Hindu temple named &quot;Pura Kawitan&quot; which often become a religious tourism destination for local tourists from Bali or cities nearby. It is believed to be one of the oldest Hindu temple in Java island. Guests that are visiting G-Land Jack&#39;s Surf Camp from Banyuwangi by car will have the chance to visit this temple along the way.</p>',
                'list_pict' => array('hundu-temple2.jpg', 'hundu-temple3.jpg'),
                ),
            
    );

$Gsetting = getSetting('en', $mysqli);

$lists_blog = getModBlog('en', $mysqli);

$lists_blog_home = getModBlog_home('en', $mysqli);