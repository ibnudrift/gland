<?php
// session_start();
error_reporting(E_ALL & ~E_NOTICE);
// error_reporting(0);
$dbcon = array(
    'host'=>'localhost',
    // 'user'=>'glandjacksurf_root1',
    // 'pass'=>'k3%O3sC+!K#G',
    // 'db'=>'glandjacksurf_neww',
    'user'=>'root',
    'pass'=>'',
    'db'=>'glandjack',
);
include 'get_setting.php';

require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

/* Global constants */
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
define('APP_PATH', dirname(ROOT_PATH).DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR);
define('ASSETS_PATH', ROOT_PATH.DIRECTORY_SEPARATOR);

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register Swiftmailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// Register URL Generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Register Validator
$app->register(new Silex\Provider\ValidatorServiceProvider());

// $app->before(function (Request $request) {
//     $url = $_SERVER['REQUEST_URI'];
//     $str_url = '';
//     if($url != '/sitondi/'){
//         $exp1 = explode('/', $url);
//         $str_url = $exp1[2];
//     }

// });
$app["twig"]->addGlobal("get_setting", $Gsetting); 
// $app["twig"]->addGlobal("url_redirect_language", $str_url);
$youtube_ids = 'rIyEN_rimz4';
$app["twig"]->addGlobal("youtube_id", $youtube_ids);

$app['list_data_Categoryp'] = $list_category_products;
$app["twig"]->addGlobal("list_data_Categoryp", $list_category_products);

$app['lists_activity'] = $lists_activity;
$app["twig"]->addGlobal("lists_activity", $lists_activity);

$app['lists_activity2'] = $lists_activity2;
$app["twig"]->addGlobal("lists_activity2", $lists_activity2);

$title_name = 'G-LAND JACK&rsquo;S SURF CAMP';
$app["twig"]->addGlobal("app_name", $title_name);

/*
$app->before(function (Request $request) use ($app) {
    if (!isset($_GET['lang'])) {
        return $app->redirect($app['url_generator']->generate('homepage').'?lang=en');
    }

    $app['twig']->addGlobal('lang_active', $_GET['lang']);

    $urls = $_SERVER['REQUEST_URI'];
    $urls = substr($urls, 0, -8);
    $app['twig']->addGlobal('current_page_name', $urls);

    // call text dual language
    $lang_message = array();
    if (isset($_GET['lang'])) {
        if ($_GET['lang'] == 'id') {
            $lang_message = include('lang/id/app.php');
        } else {
            $lang_message = include('lang/en/app.php');
        }
    }

    $app["twig"]->addGlobal("lang_message", $lang_message);
});
*/

// ------------------ Homepage ------------------------
$app->get('/', function () use ($app) {
	return $app['twig']->render('page/home.twig', array(
        'layout' => 'layouts/column1.twig',
        // 'benefits' => $app['data_benefits'],
    ));
})
->bind('homepage');

// ------------------ About ------------------
$app->get('/about', function () use ($app) {
    return $app['twig']->render('page/about.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('about');

// ------------------ activity ------------------
$app->get('/activity', function () use ($app) {

    return $app['twig']->render('page/activity.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('activity');

$app->get('/test', function () use ($app) {

    $app['swiftmailer.options'] = array(
            'host' => 'g-landjacksurfcamp.com',
            'port' => '465',
            'username' => 'no-reply@g-landjacksurfcamp.com',
            'password' => 'j@]Lr.9vIj.G',
            'encryption' => 'ssl',
            'auth_mode' => login
        );

    $pesan = \Swift_Message::newInstance()
        ->setSubject('Hi, Contact Website G-Land Jacks SurfCamp')
        ->setFrom(array('no-reply@g-landjacksurfcamp.com'))
        ->setTo( array('deoryzpandu@gmail.com') )
        ->setBody($app['twig']->render('page/mail_booking.twig', array(
            'data' => 'teks email',
        )), 'text/html');

    $app['mailer']->send($pesan);


    return true;
})
->bind('test');

// ------------------ activity detail ------------------
$app->get('/activity_detail', function () use ($app) {

    if (isset($_GET['near'])) {
        $ids = abs( (int) $_GET['near']);
        $data = $app['lists_activity2'][$ids];
    }else{
        $ids = abs( (int) $_GET['id']);
        $data = $app['lists_activity'][$ids];
    }

    return $app['twig']->render('page/activity_detail.twig', array(
        'layout' => 'layouts/inside.twig',
        'data' => $data,
    ));
})
->bind('activity_detail');

// ------------------ accomodation ------------------
$app->match('/accomodation', function (Request $request) use ($app) {

    $data = $request->get('Booking');
    if ($data == null) {
        $data = array(
            'book_room'=>'',
            'guest'=>'',
            'date_checkin'=>'',
            'date_checkout'=>'',
            'name'=>'',
            'email'=>'',
            'phone'=>'',
            'company'=>'',
            'message'=>'',
        );
    }

    if ($_POST) {
        
        $constraint = new Assert\Collection( array(
            'book_room' => new Assert\NotBlank(),
            'guest' => new Assert\Length(array('max'=>2000)),
            'email' => array(new Assert\Email(), new Assert\NotBlank()),
            'date_checkin' => new Assert\Length(array('max'=>2000)),
            'date_checkout' => new Assert\Length(array('max'=>2000)),

            'name' => new Assert\Length(array('max'=>2000)),
            'phone' => new Assert\Length(array('max'=>2000)),
            'company' => new Assert\Length(array('max'=>2000)),
            'message' => new Assert\Length(array('max'=>2000)),
        ) );

        $errors = $app['validator']->validateValue($data, $constraint);
        $errorMessage = array();

         if (!isset($_POST['g-recaptcha-response'])) {
            $errorMessage[] = 'Please Check Captcha for sending contact form!';
        }
        
        $secret_key = "6LdBKDUUAAAAAASBZ-ZZ40bhh51Ua00bipDmZSfY";
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

        $response = json_decode($response);
        if($response->success==false)
        {
            $errorMessage[] = 'Please Check Captcha for sending contact form!';
        }

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
            }
        }

        if (count($errorMessage) == 0) {
            $app['swiftmailer.options'] = array(
                    'host' => 'g-landjacksurfcamp.com',
                    'port' => '465',
                    'username' => 'no-reply@g-landjacksurfcamp.com',
                    'password' => 'j@]Lr.9vIj.G',
                    'encryption' => 'ssl',
                    'auth_mode' => login
                );

            $pesan = \Swift_Message::newInstance()
                ->setSubject('Hi, Contact Website G-Land Jacks SurfCamp')
                ->setFrom(array('no-reply@g-landjacksurfcamp.com'))
                ->setTo( array('info@g-landjacksurfcamp.com', $data['email']) )
                ->setBcc( array('deoryzpandu@gmail.com', 'ibnu@markdesign.net') )
                ->setReplyTo(array('info@g-landjacksurfcamp.com'))
                ->setBody($app['twig']->render('page/mail_booking.twig', array(
                    'data' => $data,
                )), 'text/html');

            $app['mailer']->send($pesan);

            return $app->redirect($app['url_generator']->generate('contact').'?msg=success');
        }

        // }
        // else captcha
    }

    return $app['twig']->render('page/accomodation.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('accomodation');

// ------------------ contact ---------------------------------
$app->match('/contact', function (Request $request) use ($app) {

    $data = $request->get('Contact');
    if ($data == null) {
        $data = array(
            'name'=>'',
            'email'=>'',
            'phone'=>'',
            'date_checkin'=>'',
            'date_checkout'=>'',
            'message'=>'',
        );
    }

    if ($_POST) {
        
        $constraint = new Assert\Collection( array(
            'name' => new Assert\NotBlank(),
            'phone' => new Assert\Length(array('max'=>2000)),
            'email' => array(new Assert\Email(), new Assert\NotBlank()),
            'date_checkin' => new Assert\Length(array('max'=>2000)),
            'date_checkout' => new Assert\Length(array('max'=>2000)),
            'message' => new Assert\Length(array('max'=>2000)),
        ) );

        $errors = $app['validator']->validateValue($data, $constraint);
        $errorMessage = array();

         if (!isset($_POST['g-recaptcha-response'])) {
            $errorMessage[] = 'Please Check Captcha for sending contact form!';
        }
        
        $secret_key = "6LdBKDUUAAAAAASBZ-ZZ40bhh51Ua00bipDmZSfY";
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

        $response = json_decode($response);
        if($response->success==false)
        {
            $errorMessage[] = 'Please Check Captcha for sending contact form!';
        }

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
            }
        }

        if (count($errorMessage) == 0) {
            // $app['swiftmailer.options'] = array(
            //         'host' => 'mail.gland.com',
            //         'port' => '587',
            //         'username' => 'no-reply@gland.com',
            //         'password' => 'V?#gNBE&IW6s',
            //         'encryption' => null,
            //         'auth_mode' => login
            //     );

            $pesan = \Swift_Message::newInstance()
                ->setSubject('Hello from G-Land Jacks Surf Camp')
                ->setFrom(array('no-reply@g-landjacksurfcamp.com'))
                ->setTo( array('info@g-landjacksurfcamp.com', $data['email']) )
                ->setBcc( array('deoryzpandu@gmail.com', 'ibnu@markdesign.net') )
                ->setReplyTo(array('info@g-landjacksurfcamp.com'))
                ->setBody($app['twig']->render('page/mail.twig', array(
                    'data' => $data,
                )), 'text/html');

            $app['mailer']->send($pesan);

            return $app->redirect($app['url_generator']->generate('contact').'?msg=success');
        }

        // }
        // else captcha
    }

    return $app['twig']->render('page/contactus.twig', array(
        'layout' => 'layouts/inside.twig',
        'error' => $errorMessage,
        'data' => $data,
        'msg' =>$_GET['msg'],
    ));
})
->bind('contact');


$app['lists_blog'] = $lists_blog;
$app["twig"]->addGlobal("lists_blog", $lists_blog);

$app['lists_blog_home'] = $lists_blog_home;
$app["twig"]->addGlobal("lists_blog_home", $lists_blog_home);

// ------------------ Ig ------------------
$app->get('/ig', function () use ($app) {
    return $app['twig']->render('page/ig_landing.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('ig');

// ------------------ Ig ------------------
$app->get('/ig_detail', function () use ($app) {

    $ids = intval($_GET['id']);
    $data = $app['lists_blog'][$ids];

    return $app['twig']->render('page/ig_detail.twig', array(
        'layout' => 'layouts/inside.twig',
        'data'=> $data,
    ));
})
->bind('ig_detail');


$app['debug'] = true;

$app->run();