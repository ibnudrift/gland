<?php
$session=new CHttpSession;
$session->open();
$login_member = $session['login_member'];

?>
<section class="default_sc blocks_home2 back-grey pt-0">
	<div class="prelatife container">

		<div class="block_product_data_wrap">
			<div class="top text-center">
				<div class="tag_cat">
					<ul class="list-inline">
					<?php for ($i=1; $i < 5 ; $i++) { ?>
					<?php if ($this->setting['home_product_featured_'.$i] != ''): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.id = :id');
$criteria->params[':id'] = $this->setting['home_product_featured_'.$i];
$criteria->group = 't.id';
$criteria->order = 'sort ASC';
$strCategory = PrdCategory::model()->find($criteria);
?>
						<li <?php if (($i == 1 AND $_GET['category'] == '') OR $_GET['category'] == $this->setting['home_product_featured_'.$i]): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'category'=>$strCategory->id)); ?>"><?php echo $strCategory->description->name ?></a></li>
					<?php endif ?>
					<?php } ?>
					</ul>
				</div>
			</div>
			<div id="owl-demo" class="lists_product_data row">
<?php
if ($_GET['category']) {
	$idCategory = $_GET['category'];
} else {
	$idCategory = $this->setting['home_product_featured_1'];
}
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.id = :id');
$criteria->params[':id'] = $idCategory;
$criteria->group = 't.id';
$criteria->order = 'sort ASC';
$strCategory = PrdCategory::model()->find($criteria);

$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
// $criteria->addCondition('terlaris = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->addCondition('t.tag LIKE :category');
$criteria->params[':category'] = '%category='.$idCategory.',%';
$criteria->limit = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);
?>
			<?php foreach ($dataProduct as $key => $value): ?>
	            <div class="col-md-3 col-sm-6">
	              <div class="items">
	                <div class="picture prelatife">
	                  <?php if ($value->onsale == 1): ?>
	                  <div class="boxs_inf_head_n1"></div>
	                  <?php endif ?>
	                  <?php /*<a href="<?php //echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>#">*/ ?>
	                  <?php // echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>
	                  <?php if ($_GET['category'] != ''): ?>
	                  <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id, 'category'=>$_GET['category'])); ?>">
	                  <?php else: ?>
	                  <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
	                  <?php endif ?>
	                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(273,191, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
	                  </a>
	                  <!-- </a> -->
	                </div>
	                <div class="info description">
	                  <?php if ($_GET['category'] != ''): ?>
	                  <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id, 'category'=>$_GET['category'])); ?>">
	                  <?php else: ?>
	                  <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
	                  <?php endif ?>
	                  <span class="names"><?php echo $value->description->name ?></span>
	                  </a>
	                  <div class="clear"></div>
	                  <span class="category"><?php echo $strCategory->description->name ?></span>
	                  <div class="clear"></div>
	                  <?php if ($login_member['type'] == 'member'): ?>
	                  <span style="text-decoration: line-through; color: red;"><?php echo Cart::money($value->harga) ?></span> <span class="price"><?php echo Cart::money($value->harga - (0.1 * $value->harga)) ?></span>
	                  <?php elseif ($value->harga_coret > $value->harga): ?>
	                  <span style="text-decoration: line-through; color: red;"><?php echo Cart::money($value->harga_coret) ?></span> <span class="price"><?php echo Cart::money($value->harga) ?></span>
	                  <?php else: ?>
	                  <span class="price"><?php echo Cart::money($value->harga) ?></span>
	                  <?php endif ?>
	                </div>
	              </div>
	            </div>
			<?php endforeach ?>
			</div>
			<div class="clear height-10"></div>
			<div class="block_center text-center">
				<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$idCategory)); ?>" class="btn btn-default btns_yellowsDefault">LIHAT LEBIH BANYAK</a>
			</div>
			<div class="clear"></div>
		</div>

<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('terbaru = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>

		<div class="block_product_data_wrap">
			<div class="top">
				<h6>TOP 3 KATEGORI TERFAVORIT SAAT INI</h6>
			</div>
			<div id="owl-demo2" class="lists_promotion_defaultData row categorysList">
				<?php for ($i=1; $i < 4; $i++) { ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.id = :id');
$criteria->params[':id'] = $this->setting['home_product_favorit_'.$i];
$criteria->group = 't.id';
$criteria->order = 'sort ASC';
$strCategory = PrdCategory::model()->find($criteria);
?>
				<div class="col-md-4 col-sm-6">
					<div class="items">
						<div class="picture prelatife">
							<?php // echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>
							<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$strCategory->id)); ?>">
							<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(321,321, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>
								<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(373,197, '/images/category/'.$strCategory->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img-responsive" alt="">
							</a>
						</div>
						<div class="info">
							<a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$strCategory->id)); ?>">
								<h5 class="name"><?php echo $strCategory->description->name ?></h5>
							</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description', 'category', 'brand');
$criteria->order = 'date DESC';
$criteria->addCondition('status = "1"');
$criteria->addCondition('turun_harga = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $pageSize = 12;
$criteria->group = 't.id';
$dataProduct = PrdProduct::model()->findAll($criteria);

?>

		<div class="block_product_data_wrap">
			<div class="top">
				<h6>PROMOSI TERKINI</h6>
			</div>
			<div class="clear"></div>

			<div class="lists_promotion_defaultData">
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('description.language_id = :language_id');
$criteria->addCondition('active > 0');
$criteria->params[':language_id'] = $this->languageID;
$criteria->group = 't.id';
$criteria->order = 'sort ASC';
$criteria->limit = 2;
$slide = Slide::model()->with(array('description'))->findAll($criteria);
?>

				<div class="row default">
				<?php foreach ($slide as $key => $value): ?>
					<div class="col-md-6 col-sm-6">
						<div class="items">
							<div class="picture">
							<a href="<?php echo CHtml::normalizeUrl(array('/home/promosi', 'id'=>$value->id)); ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(570,317, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a>
							</div>
							<div class="info">
								<h5><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi', 'id'=>$value->id)); ?>"><?php echo $value->description->title ?></a></h5>
								<p><?php echo $value->description->subtitle ?></p>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				</div>
			</div>
			<!-- End list promotions Data -->

			<div class="clear"></div>
		</div>

		<div class="clear height-50 hidden-xs"></div>
		<div class="clear"></div>
	</div>
</section>
<?php if ($this->setting['popup_status'] == 1): ?>
<!-- Modal -->
<div class="modal fade customsModal" id="ModalBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div> -->
      <div class="modal-body">
        <div class="ins_bodyModals text-center">
          <div class="pictures margin-bottom-0">
          	<a href="<?php echo $this->setting['popup_url'] ?>">
          		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(550,700, '/images/static/'.$this->setting['popup_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
			</a>
			</div>
          <div class="clear"></div>
          <?php // echo $this->setting['popup_content'] ?>

          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('#ModalBox').modal();
</script>
<?php endif ?>


<?php /*
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
// $.noConflict();
$(document).ready(function() {
  var owl = $("#owl-demo, #owl-demo2");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [768, 2], //2 items between 600 and 0
      itemsMobile : [600, 1], // itemsMobile disabled - inherit from itemsTablet option
  	  pagination: true,
  	  paginationNumbers: true
  });

  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })

});
</script>
<style type="text/css">
  .owl-carousel .owl-wrapper-outer{
    width: 99.6%;
  }
</style>
*/ ?>
