<?php
$e_activemenu = $this->action->id;
$controllers_ac = $this->id;
$session=new CHttpSession;
$session->open();
$login_member = $session['login_member'];

$active_menu_pg = $controllers_ac.'/'.$e_activemenu;
    $model = new ContactForm;
    $model->scenario = 'insert';

    if(isset($_POST['ContactForm']))
    {
      $model->attributes=$_POST['ContactForm'];

      if($model->validate())
      {
		  $secret_key = "6Le48yUUAAAAAH7zA92CLTWCMAsPYnUPfG-Nny5G";
		$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
		$response = json_decode($response);
		if($response->success==false)
		{
			$model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
		}else{
			// config email
			$messaged = $this->renderPartial('//mail/contact2',array(
			'model'=>$model,
			),TRUE);
			$config = array(
			'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
			'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
			'message'=>$messaged,
			);
			if ($this->setting['contact_cc']) {
			$config['cc'] = array($this->setting['contact_cc']);
			}
			if ($this->setting['contact_bcc']) {
			$config['bcc'] = array($this->setting['contact_bcc']);
			}
			// kirim email
			Common::mail($config);

			Yii::app()->user->setFlash('success_mail','Thank you for contact us. We will respond to you as soon as possible.');
			$this->refresh();
		} 
      }

    }

?>
<section class="block_bottom_home">
	<div class="prelatife container insides">
		<div class="d-inline padding-right-15 w247">
			<div class="picts">
				<img src="<?php echo $this->assetBaseurl ?>backs_bottom_humanLeft-bannerft.png" alt="" class="img-responsive">
			</div>
		</div>
		<div class="d-inline padding-top-15 mw750">
			<div class="clear height-20"></div>
			<div class="row">
				<div class="col-md-7 col-sm-7">
					<div class="tops_tx">
						<div class="d-inline">
							<p><span>HUBUNGI TIM CUSTOMER CARE PRECISE SHOES</span><br>
							<strong>
							<?php if ($this->setting['contact_phone'] != ''): ?>
								<?php echo $this->setting['contact_phone'] ?> atau
							<?php endif ?>
							<a href="mailto:<?php echo $this->setting['contact_email'] ?>"><?php echo $this->setting['contact_email'] ?></a></strong><br>
							</p>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="col-md-5 col-sm-5">
					<div class="clear height-10"></div>
					<div class="bx_text social">
						<span class="">SOSIAL MEDIA PRECISE SHOES</span><div class="clear"></div>
						<?php if ($this->setting['url_facebook'] != ''): ?>
						<a href="<?php echo $this->setting['url_facebook'] ?>" target="_blank"> <i class="fa fa-facebook"></i></a><span class="hides_mob">&nbsp;&nbsp;&nbsp;</span>
						<?php endif ?>
						<?php if ($this->setting['url_twitter'] != ''): ?>
						<a href="<?php echo $this->setting['url_twitter'] ?>" target="_blank"> <i class="fa fa-twitter"></i></a><span class="hides_mob">&nbsp;&nbsp;&nbsp;</span>
						<?php endif ?>
						<?php if ($this->setting['url_instagram'] != ''): ?>
						<a href="<?php echo $this->setting['url_instagram'] ?>" target="_blank"> <i class="fa fa-instagram"></i></a><span class="hides_mob">&nbsp;&nbsp;&nbsp;</span>
						<?php endif ?>
						<?php if ($this->setting['url_instagram_2'] != ''): ?>
						<a class="last_itm" href="<?php echo $this->setting['url_instagram_2'] ?>" target="_blank"> <i class="fa fa-instagram"></i></a>
						<?php endif ?>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			
			
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</section>


<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('parent_id = 0');
$criteria->addCondition('type = "category"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$dataCategory = PrdCategory::model()->findAll($criteria);
$dataCategory = array_chunk($dataCategory, ceil(count($dataCategory) / 2));
?>
<footer class="foot">
	<div class="tops_footer">
		<div class="prelatife container back-white">
			<div class="tengah text-center">
				<h5>PRECISE SHOES - THE PRIDE OF INDONESIAN SPORTSMEN & STREET LIFESTYLE ADDICT</h5>
				<p>Merk sepatu sport Precise Shoes adalah produk andalan Indonesia untuk aneka produk bergaya modern - lifestyle oriented, untuk seluruh kalangan yang berjiwa muda. Precise shoes merupakan tolak ukur dalam gaya fashion sepatu masa kini yang memiliki aneka koleksi seperti Gwen, Deron, Hydra dan Kakkoii yang selain berdesain stylish, elegan, namun juga memiliki teknologi seperti light weight shoes yang membuatnya sangat ringan dan power step technology yang membuat sepatu semakin awet dengan semakin seringnya dipakai. Secara khusus, sepatu sneaker sport dari Precise shoes akan memberikan peningkatan akan kekuatan, fleksibilitas dan stabilitas untuk penggunanya.</p>
			</div>

			<div class="clear height-10"></div>
			<div class="clear"></div>
		</div>
		<div class="block_ft_menuFooter">
			<ul class="list-inline">
				<li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT PRECISE SHOES</a></li>
				<li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">STORE LOCATOR</a></li>
				<li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT</a></li>
				<li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMOTION</a></li>
			</ul>
		</div>
	</div>
	<div class="backs_blue">
		<div class="prelatife container">
			<div class="row">
				<div class="col-md-3 col-sm-4">
					<p class="t-copyrights">
						Copyright &copy; 2017 Precise Shoes - Indonesia.
					</p>
				</div>
				<div class="col-md-6 col-sm-4">
					<div class="lgo_footers_wb">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl; ?>lgo_footers_wbsn.png" alt="" class="img-responsive center-block"></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-4">
					<p class="t-copyrights text-right">
						Website design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>
<script type="text/javascript">
	$(document).ready(function(){

		$('.pos_captcha_fix_abs .g-recaptcha').hide();

		$('button[type=button].buttons_subNewslet_frm').live('click', function(){
			$(this).prop('type', 'submit');
			$('.pos_captcha_fix_abs .g-recaptcha').show();
			return false;
		});
	});
</script>
<style>
	.pos_captcha_fix_abs {
		position: absolute;
		left: 10px;
		bottom: -85px;
		z-index: 50;
	}

	<?php if($active_menu_pg == 'home/pcontact'): ?>
	section.block_bottom_home{
		max-height: 30px;
		min-height: 5px;
	}
	section.block_bottom_home .container.insides{ display: none; }
	<?php endif; ?> 
</style>