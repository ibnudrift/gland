<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('parent_id = 0');
$criteria->addCondition('type = "category"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$dataCategory = PrdCategory::model()->findAll($criteria);
?>

<div class="outers_back_headers">
  <header class="head">
    <div class="visible-lg visible-md">
      <div class="tops prelatife container">
        <div class="row">
          <div class="col-md-7">
            <div class="tops_lefthdr_menu">
              <ul class="list-inline">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>" title="PROMOTION">PROMOTION</a></li>
                <li class="separator">|</li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" title="ABOUT PRECISE SHOES">ABOUT PRECISE SHOES</a></li>
                <li class="separator">|</li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>" title="STORE LOCATOR ">STORE LOCATOR </a></li>
                <li class="separator">|</li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>" title="CAREER">CAREER</a></li>
                <li class="separator">|</li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>" title="FAQ">FAQ</a></li>
                <li class="separator">|</li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>" title="CONTACT">CONTACT</a></li>
              </ul>
              <div class="clear"></div>
            </div>
            <!-- End top left head menu -->
          </div>
          <div class="col-md-5">
            <div class="rights_header prelatife">
                <div class="d-inline boxs_account_loginHeader">
                  <i class="fa fa-user"></i> &nbsp;
                  <?php if ($login_member == null): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Login</span>
                  </a>
                  &nbsp;&nbsp;|&nbsp;&nbsp;
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Daftar</span>
                  </a>
                  <?php else: ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Akun Saya</span>
                  </a>
                  &nbsp;&nbsp;|&nbsp;&nbsp;
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/logout')); ?>">
                    <span>Logout</span>
                  </a>
                  <?php endif ?>
                  <div class="clear"></div>
                </div>
                <div class="d-inline frights back-grey boxs_bCart">
                  <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>">
                    <i class="fa fa-shopping-cart"></i> &nbsp;<span><?php echo Cart::getTotalCartItem() ?> Items</span>
                  </a>
                  &nbsp;&nbsp;|&nbsp;&nbsp;
                  <b><?php echo Cart::money(Cart::getTotalCart()) ?></b>
                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
          </div>
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    <div class="bottoms">
      <div class="prelatife container">
        <div class="row p_statc">
          <div class="col-md-3">
            <div class="lgo_web_header d-inline">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl ?>lg_logo_precise.jpg" alt="" class="img-responsive">
              </a>
            </div>
            <div class="clear"></div>
          </div>
          <div class="col-md-9 p_statc">
              <div class="clear height-5"></div>
              <div class="top-menu d-inline">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$criteria->limit = 8;
$categories = PrdCategory::model()->findAll($criteria);

?>
              <ul class="list-inline">
              <?php foreach ($categories as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 'sort ASC';
$criteria->limit = 8;
$subCategory = PrdCategory::model()->findAll($criteria);

?>
                <li <?php echo (count($subCategory) > 0)? 'class="dropdown"':''; ?>><a <?php echo (count($subCategory) > 0)? 'role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown"':''; ?> href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                  <?php if (count($subCategory) > 0): ?>
                    
                  <div class="blocks_dropDown_menu_fx">
                    <ul class="list-inline">
                      <?php foreach ($subCategory as $k => $v): ?>
                        
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>">
                        <div class="picture"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(400,211, '/images/category/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="name product" class="img-responsive"></div>
                              <div class="info">
                                <span class="titles_prd"><?php echo $v->description->name ?></span>
                              </div>
                      </a></li>
                      <?php endforeach ?>
                    </ul>
                </div>
                  <?php endif ?>
                </li>
              <?php endforeach ?>
              </ul>
              <div class="clear clearfix"></div>
            </div>
            <div class="d-inline b_search">
              <form action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>" method="get">
                <div class="form-group prelatife">
                  <input type="text" class="form-control" name="q" value="<?php echo $_GET['q'] ?>" placeholder="Search">
                  <button type="submit" class="btn btn-link">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </form>
              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
        </div>
        <!-- bottoms -->
      </div>
      <div class="clear"></div>
    </div>
    </div>
    <!-- end bottom -->

      <div class="visible-sm visible-xs">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl ?>lgo_footers_wbsn.png" alt="Logo precise responsive" class="img-responsive">
              </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT PRECISE SHOES</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">PRODUCT PRECISE SHOES</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/promosi')); ?>">PROMOTION</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lokasitoko')); ?>">STORE LOCATOR </a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>">CAREER</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>">FAQ</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT</a></li>
              </ul>
              <div class="clear height-5"></div>
              <div class="bloc_aldoHeader">
                  <?php if ($login_member == null): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Login</span>
                  </a>
                  <?php else: ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">
                    <span>Akun Saya</span>
                  </a>
                  <?php endif ?>
                  <div class="boxs_bCart">
                    <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>">
                      <i class="fa fa-shopping-cart"></i> &nbsp;<span>My Cart</span>
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
              </div>

            </div>
            <!-- End menu drop -->

            <div class="blocks_menu_kategr_drop">
              <div class="menu_t">
                <a type="button" class="collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">PRODUCT PRECISE SHOES &nbsp;<i class="fa fa-chevron-down"></i></a>
              </div>
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav navbar-right">
                  <?php
                  $criteria = new CDbCriteria;
                  $criteria->with = array('description');
                  $criteria->addCondition('t.parent_id = :parent_id');
                  $criteria->params[':parent_id'] = 0;
                  $criteria->addCondition('t.type = :type');
                  $criteria->params[':type'] = 'category';
                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;
                  $criteria->order = 'sort ASC';
                  $criteria->limit = 8;
                  $categories = PrdCategory::model()->findAll($criteria);

                  ?>
                                    <?php foreach ($categories as $key => $value): ?>
                  <?php
                  $criteria = new CDbCriteria;
                  $criteria->with = array('description');
                  $criteria->addCondition('t.parent_id = :parent_id');
                  $criteria->params[':parent_id'] = $value->id;
                  $criteria->addCondition('t.type = :type');
                  $criteria->params[':type'] = 'category';
                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;
                  $criteria->order = 'sort ASC';
                  $criteria->limit = 8;
                  $subCategory = PrdCategory::model()->findAll($criteria);

                  ?>
                    <li <?php echo (count($subCategory) > 0)? 'class="dropdown"':''; ?>><a <?php echo (count($subCategory) > 0)? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"':''; ?> href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                        <?php if (count($subCategory) > 0): ?>
                          <ul class="dropdown-menu">
                          <?php foreach ($subCategory as $k => $v): ?>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                          <?php endforeach ?>
                          </ul>
                        <?php endif ?>
                    </li>
                  <?php endforeach ?>
                </ul>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </nav>
        <div class="clear"></div>
      </div>

    <div class="clear"></div>
  </header>
</div>


<script type="text/javascript">
  $(function(){
    var s_width = $(window).width();
    if (s_width > 1024){

      // $('header.head').affix({
      //   offset: {
      //     top: 250,
      //   }
      // });
      
    }
  });
</script>
<?php /*foreach($dataCategory as $key => $value): ?>
            <li>
              <a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
              <div class="blocks_dropDown_menu_fx">
                <div class="row">
  <?php
  $criteria = new CDbCriteria;
  $criteria->with = array('description');
  $criteria->addCondition('parent_id = :parent');
  $criteria->addCondition('type = "category"');
  $criteria->addCondition('description.language_id = :language_id');
  $criteria->params[':language_id'] = $this->languageID;
  $criteria->params[':parent'] = $value->id;
  $criteria->order = 'sort ASC';
  $dataCategory2 = PrdCategory::model()->findAll($criteria);
  ?>
                  <?php foreach ($dataCategory2 as $k => $val): ?>
                    
                  <div class="col-md-2 col-sm-2">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$val->id)); ?>"><h5 class="subs_mn"><?php echo $val->description->name ?></h5></a>
                    <div class="clear"></div>
                    <ul class="list-unstyled">
  <?php
  $criteria = new CDbCriteria;
  $criteria->with = array('description');
  $criteria->addCondition('parent_id = :parent');
  $criteria->addCondition('type = "category"');
  $criteria->addCondition('description.language_id = :language_id');
  $criteria->params[':language_id'] = $this->languageID;
  $criteria->params[':parent'] = $val->id;
  $criteria->order = 'sort ASC';
  $dataCategory3 = PrdCategory::model()->findAll($criteria);
  ?>
                      <?php foreach ($dataCategory3 as $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                      <?php endforeach ?>
                    </ul>
                  </div>
                  <?php if (($k + 1) % 6 == 0): ?>
                    <div class="clear"></div>
                  <?php endif ?>
                  <?php endforeach ?>
                </div>
              </div>
            </li>
            <?php endforeach ?>
*/ ?>

<?php /*
<script type="text/javascript">
  $(document).ready(function(){
      $('.nl_popup a').live('hover', function(){
          $('.popup_carts_header').fadeIn();
      });
      $('.popup_carts_header').live('mouseleave', function(){
        setTimeout(function(){ 
            $('.popup_carts_header').fadeOut();
        }, 500);
      });
  });
</script>

<script type="text/javascript">
    $(function(){
      $('#myAffix').affix({
        offset: {
          top: 300
        }
      })
    })
  </script>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-5"></div>
  <div class="prelatife container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <div class="lgo-web-web_affix">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl ?>lgo_stumble_wt_txt.png" alt="" class="img-responsive d-inline" style="max-width: 75px;">
            STUMBLE UPON
          </a>
        </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="text-right"> 
          <div class="clear height-20"></div>
          <div class="menu-taffix">
            <ul class="list-inline">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">BUY COFFEE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/howto')); ?>">HOW TO ORDER</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/wholesale')); ?>">WHOLESALE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/ ?>