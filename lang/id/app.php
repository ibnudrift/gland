<?php 

return array(
		'HOME' => 'BERANDA',
		'COMPANY' => 'PROFIL PERUSAHAAN',
		'INDONESIAN_SPICES' => 'REMPAH &amp; KOMODITI INDONESIA',
		'MARKETS' => 'PASAR',
		'CONTACT_US' => 'HUBUNGI KAMI',
		
		// contact form
		'Name'=>'Nama',
		'Company'=>'Perusahaan',
		'Email'=>'Email',
		'Phone'=>'Telepon',
		'Address'=>'Indonesian Spices & Commodities Interest',
		'Message'=>'Pesan',
	);


?>