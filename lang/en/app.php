<?php 

return array(
		'HOME' => 'HOME',
		'COMPANY' => 'COMPANY PROFILE',
		'INDONESIAN_SPICES' => 'INDONESIAN SPICES & COMMODITIES',
		'MARKETS' => 'MARKETS',
		'CONTACT_US' => 'CONTACT US',

		// contact form
		'Name'=>'Name',
		'Company'=>'Company',
		'Email'=>'Email',
		'Phone'=>'Phone',
		'Address'=>'Indonesian Spices & Commodities Interest',
		'Message'=>'Message',
	);


?>